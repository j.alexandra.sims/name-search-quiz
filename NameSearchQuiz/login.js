
var userNames = ['aphrodite', 'artemis', 'athena', 'demeter', 'hades', 'hera', 'hermes', 'poseidon', 'serena', 'zeus'];
var listLength = userNames.length;

// function to get length of any array. May or may not be necessary given the efficiency of the length function.
/*
function getArrayLength(arrayName) {
  var arrayLength = arrayName.length;
  return arrayLength;
}
*/

// Capitalizes the first character of a string
function capitalizeFirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

// Search for user-defined name within pre-loaded username array.
function findName () {
  var name = prompt('Search for user name: (aphrodite, artemis, athena, demeter, hades, hera, hermes, poseidon, serena, zeus');
  name = name.toLowerCase();
  for (var i = listLength; i >= 0; i -= 1) {
    console.log(userNames[i]);
    if ( userNames[i] === name ) {
      alert('Success! The username, ' + capitalizeFirst(name) + ' is located at index ' + i + ' ' + userNames.indexOf(name));
      document.write(capitalizeFirst(name) + ' located at ' + ' index ' + userNames.indexOf(name) + '<br /br>');
      document.write('<button onclick=document.location.reload()>Click to Restart</button>');
      break;
    }
    else if ( i === 0 ) {
      alert('Sorry, that username is not on the list.')
      document.write('<button onclick=document.location.reload()>Click to Restart</button>');
    }
  }
}

findName();